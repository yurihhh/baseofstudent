package sample;

import java.util.Comparator;

public class StudLastNameComparator<T> implements Comparator<T> {

	public int compare(Object a, Object b) {
		Student stud1 = (Student) a;
		Student stud2 = (Student) b;
		
		int lastNameCompare = stud1.getLastName().compareTo(stud2.getLastName());

		int nameCompare = stud1.getName().compareTo(stud2.getName());

		return (lastNameCompare == 0) ? nameCompare : lastNameCompare;

		

//		String lastName1 = stud1.getLastName();
//		String lastName2 = stud2.getLastName();
//
//		return lastName1.compareTo(lastName2);

	}

}
