
1.Задание:

1) Создать класс Human.
Поля:
● String name (имя)
● String lastName (фамилия)
● Gender gender (пол. Реализовать с помощью Enum)
 Методы:
● Стандартные (методы получения и установки, toString() и т. д.)
2) Создать класс Student как подкласс Human.
Поля:
● int id (номер зачетки)
● String groupName (название группы где он учится)
Методы:
● Стандартные (методы получения и установки, toString() и т. д.)
3) Создать классы GroupOverflowException, StudentNotFoundException (наследники Exception) в качестве пользовательских
исключений.
4) Создать класс Group
Поля:
● String groupName (название)
● Student[] studens = new Student[10]; (массив из 10 студентов)
 Методы:
● Стандартные (методы получения и установки, toString() и т. д.)
● public void addStudent(Student student) throws GroupOverflowException (метод добавления студента в группу. В случае
добавления 11 студента должно быть возбужденно пользовательское исключение)
● public Student searchStudentByLastName(String lastName) throws StudentNotFoundException (метод поиска студента в группе.
Если студент не найден должно быть возбужденно пользовательское исключение)
● public boolean removeStudentByID(int id) (метод удаления студента по номеру зачетки, вернуть true если такой студент был и он
был удален и false в противном случае)


2.Задание:

1) Объявите интерфейс
public interface StringConverter{
public String toStringRepresentation (Student student);
public Student fromStringRepresentation (String str);
 }
2) Объявите класс CSVStringConverter реализующий указанный интерфейс. Логика реализации
следующая — на основе Студента создать строку с его CSV представлением и наоборот на основе
этой строки создать Студента.


3.Задание:

1) Напишите программу, которая скопирует все файлы с заранее определенным расширением
(например, только doc) из одного каталога в другой.
2) Реализуйте отдельный класс GroupFileStorage в котором будут следующие методы:
● void saveGroupToCSV(Group gr) — запись группы в CSV файл
● Group loadGroupFromCSV(File file) — вычитка и возврат группы из файла
● File findFileByGroupName(String groupName, File workFolder) — поиск файла в рабочем каталоге
(workFolder). Название файла определяется названием группы в нем сохраненной.



4.Задание:
1)Реализуйте корректные методы equals, hashCode для классов Человек, Студент и
Группа.
2)Реализуйте вспомогательный метод для проверки факта отсутствия эквивалентных
студентов в группе. 

5.Задание:
Модифицируйте класс «Группа» для более удобных методов
работы с динамическими массивами.