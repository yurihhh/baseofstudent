//package sample;
//
//import java.util.Arrays;
//import java.util.Comparator;
//import java.util.Objects;
//
//public class OldGroup {
//	private String groupName;
////	private final List<Student> students = new ArrayList<>();
//	private final Student[] students = new Student[10];
//
//	public OldGroup(String groupName) {
//		super();
//		this.groupName = groupName;
//	}
//
//	public OldGroup() {
//		super();
//	}
//
//	public String getGroupName() {
//		return groupName;
//	}
//
//	public void setGroupName(String groupName) {
//		this.groupName = groupName;
//	}
//
//	public Student[] getStudents() {
//		return students;
//	}
//	
//	
//
//	
//
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + Arrays.hashCode(students);
//		result = prime * result + Objects.hash(groupName);
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Group other = (Group) obj;
//		return Objects.equals(groupName, other.groupName) && Arrays.equals(students, other.students);
//	}
//
//	public void addStudent(Student student) throws GroupOverflowException {
//		for (int i = 0; i < students.length; i++) {
//			if (students[i] == null) {
//				students[i] = student;
//				student.setGroupName(this.groupName);
//				return;
//			}
//		}
//		throw new GroupOverflowException("Group overflow");
//	}
//
//	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
//		for (int i = 0; i < students.length; i++) {
//			if (students[i] != null) {
//				String studentLastName = students[i].getLastName();
//				if (studentLastName.equals(lastName)) {
//					return students[i];
//				}
//			}
//		}
//		throw new StudentNotFoundException("Student doesn't exist");
//	}
//
//	public boolean removeStudentById(int id) {
//		for (int i = 0; i < students.length; i++) {
//			if (students[i] != null) {				
//				if (students[i].getId() == id) {
//					students[i] = null;
//					System.out.println("Student was removed from group");
//					return true;
//				}
//			}
//		}
//		return false;
//	}
//
//	@Override
//	public String toString() {
//		String group = "Group " + groupName + ":" +System.lineSeparator();
//
//		for (int i = 0; i < students.length; i++) {
//			if(students[i]!=null) {
//				group += students[i]+System.lineSeparator();
//			}
//		}
//		return group;
//	}
//	
//	public void hasEquivalentStudents() {
//		boolean result = false;
//		for (int i = 0; i < students.length - 1; i++) {
//			for (int j = i + 1; j < students.length; j++) {
//				if (students[i] != null && students[j] != null) {
//					if (students[i].equals(students[j])) {
//						result = true;
//						break;
//					}
//				}
//			}
//		}
//		if (result) {
//			System.out.println("Group " + groupName + " has duplicated Student");
//		} else {
//			System.out.println("Group " + groupName + " doesn't have duplicated students");
//		}
//	}
//
//	
//	public void sortStudentsByLastName() {
//		Arrays.sort(students, Comparator.nullsLast(new StudLastNameComparator()));
//	}
//}
