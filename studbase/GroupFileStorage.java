package sample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

public class GroupFileStorage {
	
	public void saveGroupToCSV(Group gr) {
		File group = new File("C:\\workspace\\StudentBase\\src\\sample\\" + gr.getGroupName() + ".csv");
		List<Student> students = gr.getStudents();
		String result = "";

		try (PrintWriter pw = new PrintWriter(group)) {
			for (Student st : students) {
					result += StringConverter.toStringRepresentation(st) + System.lineSeparator();
				
			}
			pw.print(result);
		} catch (FileNotFoundException e) {
			System.out.println("You got problem with file to write!");

		}

	}

	public Group loadGroupFromCSV(File file) {
		Group st = new Group(file.getName().substring(0, file.getName().indexOf(".")));
		
		try(Scanner sc = new Scanner(file)){
			while (sc.hasNextLine()) {
				st.addStudent(StringConverter.fromStringRepresentation(sc.nextLine()));
			}
		} catch (FileNotFoundException e) {
		} catch (GroupOverflowException e) {
		}
		return st;
	}

	public File findFileByGroupName(String groupName, File workFolder) {
		if (workFolder.isDirectory()) {
			File[] files = workFolder.listFiles();

			for (int i = 0; i < files.length; i++) {
				if (files[i].getName().contains(groupName)) {
					return files[i].getAbsoluteFile();
				}
			}
		}
		return null;

	}

}
