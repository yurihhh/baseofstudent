package sample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Group {
	private String groupName;
	private final List<Student> students = new ArrayList<>();
	

	public Group(String groupName) {
		super();
		this.groupName = groupName;
	}

	public Group() {
		super();
	}


	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<Student> getStudents() {
		return students;
	}

	

	@Override
	public int hashCode() {
		return Objects.hash(groupName, students);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		return Objects.equals(groupName, other.groupName) && Objects.equals(students, other.students);
	}

	public void addStudent(Student student) throws GroupOverflowException {
		if (students.size() < 10) {
			students.add(student);
			student.setGroupName(this.groupName);
			return;
		}
		throw new GroupOverflowException("Group overflow");
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		for (Student stud : students) {
			if (stud.getLastName().contains(lastName)) {
				return stud;
			}
		}
		throw new StudentNotFoundException("Student doesn't exist");
	}

	public boolean removeStudentById(int id) {
		for (Student stud : students) {
			if (stud.getId() == id) {
				students.remove(stud);
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		sortStudentsByLastName();
		String group = "Group " + groupName + ":" + System.lineSeparator();
		for (Student stud : students) {
			group += stud + System.lineSeparator();
		}
		return group;
	}
	
	public void hasEquivalentStudents() {
		boolean result = false;
		for (int i = 0; i < students.size() - 1; i++) {
			for (int j = i + 1; j < students.size(); j++) {
				if (students.get(i).equals(students.get(j))) {
					result = true;
					break;
					}
				}
			}
		
		if (result) {
			System.out.println("Group " + groupName + " has duplicated Student");
		} else {
			System.out.println("Group " + groupName + " doesn't have duplicated students");
		}
	}

	
	public void sortStudentsByLastName() {
		Collections.sort(students, Comparator.nullsLast(new StudLastNameComparator<Student>()));
	}




}
