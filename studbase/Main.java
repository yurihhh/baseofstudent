package sample;

import java.io.File;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		
		GroupFileStorage storage = new GroupFileStorage();
		File academy = new File ("C:\\workspace\\StudentBase\\src\\sample\\");
		AddStudent add = new AddStudent();
		
		Student h1 = new Student("Viktoria", "Kusokova", Gender.FEMALE, 101, "Maths");
		Student h2 = new Student("Viktor", "Pelena", Gender.MALE, 102, "Maths");
		Student h3 = new Student("Mykyta", "Averkin", Gender.MALE, 103, "Maths");
		Student h4 = new Student("Olha", "Lisenko", Gender.FEMALE, 104, "Maths");
		Student h5 = new Student("Vitaliy", "Buga", Gender.MALE, 105, "Maths");
		Student j1 = new Student("Anastasiya", "Zhdanova", Gender.FEMALE, 106, "Physics");
		Student j2 = new Student("Dmitriy", "Grigorenko", Gender.MALE, 107, "Physics");
		Student j3 = new Student("Bogdan", "Cirpak", Gender.MALE, 109, "Physics");
		Student j4 = new Student("Diana", "Lipa", Gender.FEMALE, 110, "Physics");
		Student j5 = new Student("Petro", "Duma", Gender.MALE, 111, "Physics");

		
		Group mathsGroup = new Group("Maths");
		Group physicsGroup = new Group("Physics");
		
		try {
			mathsGroup.addStudent(h1);
			mathsGroup.addStudent(h2);
			mathsGroup.addStudent(h3);
			mathsGroup.addStudent(h4);
			mathsGroup.addStudent(h5);
			physicsGroup.addStudent(j1);
			physicsGroup.addStudent(j2);
			physicsGroup.addStudent(j3);
			physicsGroup.addStudent(j4);
			physicsGroup.addStudent(j5);
		} catch (GroupOverflowException e) {
			e.printStackTrace();
			System.out.println("Group is full");
		}
		System.out.println(mathsGroup);
		System.out.println(physicsGroup);
		
		
		
		mathsGroup.sortStudentsByLastName();
		physicsGroup.sortStudentsByLastName();
		
	
		System.out.println(physicsGroup.removeStudentById(107) + System.lineSeparator());
		System.out.println(mathsGroup.removeStudentById(103) + System.lineSeparator());
		
		try {
			Student finder = mathsGroup.searchStudentByLastName("Buga");
			System.out.println("Student is found: " + System.lineSeparator()+finder);
		} catch (StudentNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(System.lineSeparator() + "Edit " + mathsGroup);
		System.out.println(System.lineSeparator() + "Edit " + physicsGroup);
		
		
		
//		Student b1 = add.newStudent();
		
		
        storage.saveGroupToCSV(mathsGroup);
        storage.saveGroupToCSV(physicsGroup);
		
		File importMathsGroup = storage.findFileByGroupName("Maths", academy);
		File importPhysicsGroup = storage.findFileByGroupName("Physics", academy);
		
		Group newMathsGroup = storage.loadGroupFromCSV(importMathsGroup);
		Group newPhysicsGroup = storage.loadGroupFromCSV(importPhysicsGroup);
		
		System.out.println(newMathsGroup);
		System.out.println(newPhysicsGroup);

		
		mathsGroup.hasEquivalentStudents();
		physicsGroup.hasEquivalentStudents();
		
		
	}



	}


